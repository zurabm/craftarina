package com.craftarina.webstore.runner.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan(basePackages = {"com.craftarina.webstore.infra.jpa.entities"})
@EnableJpaRepositories(basePackages = {"com.craftarina.webstore.infra.jpa.daos"})
@SpringBootApplication(scanBasePackages = {"com.craftarina.webstore.infra"})
public class WebstoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebstoreApplication.class, args);
    }

}
