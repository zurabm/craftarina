package com.craftarina.webstore.infra.spring.controllers.sessions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchSessionRequest {
    private String username;
    private String password;
}
