package com.craftarina.webstore.infra.spring.controllers.help;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(HelpController.ROOT)
public class HelpController {
    static final String ROOT = "/help";
    static final String FETCH_HELP = "";

    public static final String FETCH_HELP_URL = ROOT + FETCH_HELP;

    @GetMapping(FETCH_HELP)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public FetchHelpResponse fetchHelp() {
        return new FetchHelpResponse(
                "Hello, this is a small web-store for hand-knitted goods. " +
                        "It's not much yet, but anticipate improvements soon!"
        );
    }
}
