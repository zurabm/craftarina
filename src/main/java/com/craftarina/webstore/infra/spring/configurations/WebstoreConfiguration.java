package com.craftarina.webstore.infra.spring.configurations;

import com.craftarina.webstore.core.products.ProductInteractor;
import com.craftarina.webstore.core.repositories.IProductRepository;
import com.craftarina.webstore.core.repositories.IUserRepository;
import com.craftarina.webstore.core.services.CraftarinaService;
import com.craftarina.webstore.core.users.UserInteractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebstoreConfiguration {
    @Bean
    public CraftarinaService getCraftarinaService(IUserRepository userRepository,
                                                  IProductRepository productRepository) {
        return new CraftarinaService(
                new UserInteractor(userRepository),
                new ProductInteractor(productRepository)
        );
    }
}
