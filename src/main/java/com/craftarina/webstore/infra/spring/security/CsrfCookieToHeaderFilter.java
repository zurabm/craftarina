package com.craftarina.webstore.infra.spring.security;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

import static com.craftarina.webstore.infra.spring.configurations.SecurityConfiguration.CSRF_HEADER_NAME;
import static com.craftarina.webstore.infra.spring.controllers.sessions.SessionController.CSRF_COOKIE_NAME;

public class CsrfCookieToHeaderFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(@NotNull HttpServletRequest request,
                                    @NotNull HttpServletResponse response,
                                    @NotNull FilterChain filterChain) throws ServletException, IOException {
        if (hasCsrfToken(request)) {
            String token = getCsrfToken(request);
            request = new HttpServletRequestWrapper(request) {
                @Override
                public String getHeader(String name) {
                    if (name.equals(CSRF_HEADER_NAME)) {
                        return token;
                    } else {
                        return super.getHeader(name);
                    }
                }
            };
        }

        filterChain.doFilter(request, response);
    }

    /**
     * Returns true if a CSRF Token is present in the request as a cookie.
     */
    private static boolean hasCsrfToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();

        return Arrays.stream(cookies == null ? new Cookie[0] : cookies)
                .map(Cookie::getName)
                .anyMatch(cookie -> cookie.equals(CSRF_COOKIE_NAME));
    }

    /**
     * Returns the CSRF token that is included as a cookie in the specified request.
     */
    private static String getCsrfToken(HttpServletRequest request) {
        assert hasCsrfToken(request) : "A CSRF cookie is not present in the request";

        return Arrays.stream(request.getCookies())
                .filter(cookie -> cookie.getName().equals(CSRF_COOKIE_NAME))
                .map(Cookie::getValue).findFirst().orElseThrow();
    }
}
