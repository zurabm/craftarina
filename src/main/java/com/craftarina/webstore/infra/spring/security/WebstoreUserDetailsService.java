package com.craftarina.webstore.infra.spring.security;

import com.craftarina.webstore.core.services.CraftarinaService;
import com.craftarina.webstore.core.users.InvalidUsernameException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class WebstoreUserDetailsService implements UserDetailsService {
    public static final Logger LOGGER = LoggerFactory.getLogger(WebstoreUserDetailsService.class);

    public WebstoreUserDetailsService(CraftarinaService craftarinaService) {
        this.craftarinaService = craftarinaService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("Retrieving user details for '{}'.", username);

        try {
            return WebstoreUserDetails.of(craftarinaService.getUser(username));
        } catch (InvalidUsernameException e) {
            LOGGER.info("Couldn't load user by username. {}", e.getMessage());
            throw new UsernameNotFoundException("The user '%s' does not exist".formatted(username));
        }
    }

    private final CraftarinaService craftarinaService;
}
