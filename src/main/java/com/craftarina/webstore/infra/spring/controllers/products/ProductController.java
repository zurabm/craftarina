package com.craftarina.webstore.infra.spring.controllers.products;

import com.craftarina.webstore.core.products.MissingImageException;
import com.craftarina.webstore.core.services.CraftarinaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(ProductController.ROOT)
public class ProductController {
    public static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    static final String ROOT = "/products";
    static final String FETCH_PRODUCTS = "";
    static final String ADD_PRODUCT = "";

    static final String IMAGES = "/images";
    static final String FETCH_PRODUCT_IMAGE = IMAGES + "/{productName}";
    static final String ADD_PRODUCT_IMAGE = IMAGES + "/{productName}";

    public static final String FETCH_PRODUCTS_URL = ROOT + FETCH_PRODUCTS;
    public static final String ADD_PRODUCT_URL = ROOT + ADD_PRODUCT;
    public static final String FETCH_PRODUCT_IMAGE_URL = ROOT + FETCH_PRODUCT_IMAGE;
    public static final String ADD_PRODUCT_IMAGE_URL = ROOT + ADD_PRODUCT_IMAGE;

    public ProductController(CraftarinaService craftarinaService) {
        this.craftarinaService = craftarinaService;
    }

    /**
     * Fetches all currently available products from the store's catalog.
     */
    @GetMapping(FETCH_PRODUCTS)
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public FetchProductsResponse fetchProducts() {
        List<ProductModel> products = new ArrayList<>();
        for (var product : craftarinaService.getProducts()) {
            products.add(ProductModel.from(product));
        }

        return new FetchProductsResponse(products);
    }

    /**
     * Adds the specified product to the store's catalog.
     */
    @PostMapping(ADD_PRODUCT)
    @ResponseStatus(HttpStatus.CREATED)
    public void addProduct(@RequestBody AddProductRequest request) {
        craftarinaService.addProduct(request.getProduct().toProduct());
    }

    /**
     * Fetches the image corresponding to the specified product.
     */
    @GetMapping(FETCH_PRODUCT_IMAGE)
    @ResponseStatus(HttpStatus.OK)
    public FetchProductImageResponse fetchProductImage(@PathVariable String productName) {
        return new FetchProductImageResponse(craftarinaService.getProductImage(productName).array());
    }

    /**
     * Adds an image for the specified product.
     */
    @PostMapping(ADD_PRODUCT_IMAGE)
    @ResponseStatus(HttpStatus.CREATED)
    public void addProductImage(@PathVariable String productName, @RequestBody AddProductImageRequest request) {
        craftarinaService.addProductImage(productName, ByteBuffer.wrap(request.getImageBytes()));
    }

    @ExceptionHandler(MissingImageException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleMissingImageException(MissingImageException e) {
        LOGGER.error("Could not find a product image. Exception: {}", e.getMessage());
    }

    private final CraftarinaService craftarinaService;
}
