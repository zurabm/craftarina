package com.craftarina.webstore.infra.spring.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public record RequestMethodMatcher(HttpMethod method, Set<String> uris) implements RequestMatcher {
    public RequestMethodMatcher(HttpMethod method, String... uris) {
        this(method, Arrays.stream(uris).collect(Collectors.toSet()));
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        return request.getMethod().equals(method.name()) && uris.contains(request.getRequestURI());
    }
}
