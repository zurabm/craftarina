package com.craftarina.webstore.infra.spring.controllers.products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchProductImageResponse {
    private byte[] imageBytes;
}
