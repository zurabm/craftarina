package com.craftarina.webstore.infra.spring.configurations;

public class Profiles {
    public static final String PERSISTENCE_PROFILE = "persistence";
    public static final String SECURITY_PROFILE = "security";
}
