package com.craftarina.webstore.infra.spring.configurations;

import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.infra.spring.security.CsrfCookieToHeaderFilter;
import com.craftarina.webstore.infra.spring.security.RequestMethodMatcher;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import static com.craftarina.webstore.infra.spring.configurations.Profiles.SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.controllers.products.ProductController.ADD_PRODUCT_IMAGE_URL;
import static com.craftarina.webstore.infra.spring.controllers.products.ProductController.ADD_PRODUCT_URL;
import static com.craftarina.webstore.infra.spring.controllers.sessions.SessionController.FETCH_SESSION_URL;
import static com.craftarina.webstore.infra.spring.controllers.users.UserController.ADD_USER_URL;

@EnableWebSecurity
@Profile(SECURITY_PROFILE)
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    public static final String CSRF_HEADER_NAME = "X-XSRF-TOKEN";

    public SecurityConfiguration(CsrfTokenRepository csrfTokenRepository) {
        this.csrfTokenRepository = csrfTokenRepository;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Authorization Configuration.
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, ADD_PRODUCT_URL).hasAuthority(Role.ADMIN.getRoleName())
                .antMatchers(HttpMethod.POST, ADD_PRODUCT_IMAGE_URL).hasAuthority(Role.ADMIN.getRoleName());

        // CSRF Configuration.
        http.csrf()
                .csrfTokenRepository(csrfTokenRepository)
                .ignoringRequestMatchers(new RequestMethodMatcher(
                        HttpMethod.POST,
                        ADD_USER_URL, FETCH_SESSION_URL
                ))
                .and()
                .addFilterBefore(new CsrfCookieToHeaderFilter(), CsrfFilter.class);
    }

    @Override
    public void configure(WebSecurity web) {
        web.debug(true);
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public static PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public static CsrfTokenRepository getCsrfTokenRepository() {
        HttpSessionCsrfTokenRepository csrfTokenRepository = new HttpSessionCsrfTokenRepository();
        csrfTokenRepository.setHeaderName(CSRF_HEADER_NAME);

        return csrfTokenRepository;
    }

    @Bean
    public static WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(@NotNull CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("http://localhost:3000");
            }
        };
    }

    private final CsrfTokenRepository csrfTokenRepository;
}
