package com.craftarina.webstore.infra.spring.security;

import com.craftarina.webstore.core.users.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class WebstoreUserDetails implements UserDetails {
    /**
     * Creates a {@link WebstoreUserDetails} object from the specified user details.
     */
    public static WebstoreUserDetails of(String username, String password, RoleAuthority roleAuthority) {
        return new WebstoreUserDetails(username, password, roleAuthority);
    }

    /**
     * Creates a {@link WebstoreUserDetails} object for the specified {@link User}.
     */
    public static WebstoreUserDetails of(User user) {
        return new WebstoreUserDetails(user.username(), user.password(), RoleAuthority.fromRole(user.role()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singletonList(roleAuthority);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    private WebstoreUserDetails(String username, String password, RoleAuthority roleAuthority) {
        this.username = username;
        this.password = password;
        this.roleAuthority = roleAuthority;
    }

    private final String username;
    private final String password;
    private final RoleAuthority roleAuthority;
}
