package com.craftarina.webstore.infra.spring.controllers.products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchProductsResponse {
    private List<ProductModel> products;
}
