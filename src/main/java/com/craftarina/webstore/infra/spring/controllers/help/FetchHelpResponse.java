package com.craftarina.webstore.infra.spring.controllers.help;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FetchHelpResponse {
    private String helpMessage;
}
