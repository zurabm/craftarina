package com.craftarina.webstore.infra.spring.controllers.users;

import com.craftarina.webstore.core.services.CraftarinaService;
import com.craftarina.webstore.core.users.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(UserController.ROOT)
public class UserController {
    static final String ROOT = "/users";
    static final String ADD_USER = "";

    public static final String ADD_USER_URL = ROOT + ADD_USER;

    public UserController(CraftarinaService craftarinaService, PasswordEncoder encoder) {
        this.craftarinaService = craftarinaService;
        this.encoder = encoder;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void addUser(@RequestBody AddUserRequest request) {
        craftarinaService.addUser(new User(request.getUsername(), encoder.encode(request.getPassword())));
    }

    private final CraftarinaService craftarinaService;
    private final PasswordEncoder encoder;
}
