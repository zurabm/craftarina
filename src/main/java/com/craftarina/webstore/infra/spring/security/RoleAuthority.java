package com.craftarina.webstore.infra.spring.security;

import com.craftarina.webstore.core.users.Role;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;

public enum RoleAuthority implements GrantedAuthority {
    USER(Role.USER),
    ADMIN(Role.ADMIN);

    /**
     * Returns the {@link RoleAuthority} enum corresponding to the specified {@link Role}.
     */
    public static RoleAuthority fromRole(Role role) {
        return Arrays.stream(RoleAuthority.values())
                .filter(authority -> authority.role.equals(role))
                .findFirst().orElseThrow();
    }

    @Override
    public String getAuthority() {
        return role.getRoleName();
    }

    RoleAuthority(Role role) {
        this.role = role;
    }

    private final Role role;
}
