package com.craftarina.webstore.infra.spring.controllers.products;

import com.craftarina.webstore.core.products.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductModel {
    private String name;
    private double price;
    private String description;
    private int stock;

    /**
     * Returns a {@link Product} from this DTO.
     */
    public Product toProduct() {
        return new Product(getName(), getPrice(), getDescription(), getStock());
    }

    /**
     * Creates a {@link ProductModel} to be used as a DTO, from the specified {@link Product}.
     */
    public static ProductModel from(Product product) {
        return new ProductModel(product.name(), product.price(), product.description(), product.stock());
    }
}
