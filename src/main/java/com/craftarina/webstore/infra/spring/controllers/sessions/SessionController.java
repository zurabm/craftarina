package com.craftarina.webstore.infra.spring.controllers.sessions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(SessionController.ROOT)
public class SessionController {
    public static final Logger LOGGER = LoggerFactory.getLogger(SessionController.class);

    static final String ROOT = "/sessions";
    static final String FETCH_SESSION = "";

    public static final String FETCH_SESSION_URL = ROOT + FETCH_SESSION;
    public static final String CSRF_COOKIE_NAME = "XSRF-TOKEN";

    public SessionController(AuthenticationManager authenticationManager, CsrfTokenRepository csrfTokenRepository) {
        this.authenticationManager = authenticationManager;
        this.csrfTokenRepository = csrfTokenRepository;
    }

    @PostMapping(FETCH_SESSION)
    @ResponseStatus(HttpStatus.OK)
    public void fetchSession(HttpServletRequest servletRequest,
                             HttpServletResponse servletResponse,
                             @RequestBody FetchSessionRequest request) throws BadCredentialsException {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword())
        );

        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authentication);
        SecurityContextHolder.setContext(context);

        Cookie csrfTokenCookie = new Cookie(CSRF_COOKIE_NAME, csrfTokenRepository.loadToken(servletRequest).getToken());
        csrfTokenCookie.setHttpOnly(true);

        servletResponse.addCookie(csrfTokenCookie);
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public void handleBadCredentialsException(BadCredentialsException e) {
        LOGGER.info("Received invalid credentials. Exception: {}", e.getMessage());
    }

    private final AuthenticationManager authenticationManager;
    private final CsrfTokenRepository csrfTokenRepository;
}
