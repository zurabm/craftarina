package com.craftarina.webstore.infra.repositories;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.repositories.IProductRepository;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryProductRepository implements IProductRepository {
    public InMemoryProductRepository() {
        products = new ArrayList<>();
        productImages = new HashMap<>();
    }

    @Override
    public void addProduct(Product product) {
        assert !hasProduct(product.name()) : "A product with the name '%s' already exists".formatted(product.name());

        products.add(product);
    }

    @Override
    public void addProductImage(String productName, ByteBuffer productImage) {
        assert hasProduct(productName) : "A product with the name '%s' does not exist".formatted(productName);
        assert !hasProductImage(productName) : "The product '%s' already has an image.".formatted(productName);

        productImages.put(productName, productImage);
    }

    @Override
    public boolean hasProductImage(String productName) {
        assert hasProduct(productName) : "A product with the name '%s' does not exist".formatted(productName);

        return productImages.containsKey(productName);
    }

    @Override
    public ByteBuffer getProductImage(String productName) {
        assert hasProduct(productName) : "A product with the name '%s' does not exist".formatted(productName);
        assert hasProductImage(productName) : "The product '%s' does not have an image.".formatted(productName);

        return productImages.get(productName);
    }

    @Override
    public Iterable<Product> getProducts() {
        return products;
    }

    /**
     * Clears all product information from this repository.
     */
    public void clearProducts() {
        products.clear();
        productImages.clear();
    }

    /**
     * Returns true if a product with the specified name exists.
     */
    private boolean hasProduct(String productName) {
        return products.stream().anyMatch(product -> product.name().equals(productName));
    }

    private final List<Product> products;
    private final Map<String, ByteBuffer> productImages;
}
