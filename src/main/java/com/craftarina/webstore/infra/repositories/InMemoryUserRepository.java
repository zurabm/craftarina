package com.craftarina.webstore.infra.repositories;

import com.craftarina.webstore.core.repositories.IUserRepository;
import com.craftarina.webstore.core.users.User;

import java.util.HashMap;
import java.util.Map;

public class InMemoryUserRepository implements IUserRepository {
    public InMemoryUserRepository() {
        users = new HashMap<>();
    }

    @Override
    public void addUser(User user) {
        assert !hasUser(user.username()) : "A user with the name '%s' already exists.".formatted(user.username());

        users.put(user.username(), user);
    }

    @Override
    public boolean hasUser(String username) {
        return users.containsKey(username);
    }

    @Override
    public User getUser(String username) {
        assert hasUser(username) : "A user with the name '%s' does not exist.".formatted(username);

        return users.get(username);
    }

    /**
     * Clears all user information from this repository.
     */
    public void clear() {
        users.clear();
    }

    private final Map<String, User> users;
}
