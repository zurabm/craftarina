package com.craftarina.webstore.infra.repositories;

import com.craftarina.webstore.core.carts.Cart;
import com.craftarina.webstore.core.repositories.ICartRepository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryCartRepository implements ICartRepository {
    public InMemoryCartRepository() {
        this.carts = new HashMap<>();
    }

    @Override
    public void addCart(String username, Cart cart) {
        assert !hasCart(username) : "The user '%s' already has an active shopping cart".formatted(username);

        carts.put(username, cart);
    }

    @Override
    public boolean hasCart(String username) {
        return carts.containsKey(username);
    }

    @Override
    public Cart getCart(String username) {
        assert hasCart(username) : "The user '%s' does not have an active cart.".formatted(username);

        return carts.get(username);
    }

    @Override
    public void updateCart(String username, Cart cart) {
        assert hasCart(username) : "The user '%s' does not have an active cart.".formatted(username);

        carts.put(username, cart);
    }

    @Override
    public void removeCart(String username) {
        assert hasCart(username) : "The user '%s' does not have an active cart.".formatted(username);

        carts.remove(username);
    }

    private final Map<String, Cart> carts;
}
