package com.craftarina.webstore.infra.jpa.repositories;

import com.craftarina.webstore.core.repositories.IUserRepository;
import com.craftarina.webstore.core.users.User;
import com.craftarina.webstore.infra.jpa.daos.JpaUserDao;
import com.craftarina.webstore.infra.jpa.entities.UserEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import static com.craftarina.webstore.infra.spring.configurations.Profiles.PERSISTENCE_PROFILE;


@Repository
@Profile(PERSISTENCE_PROFILE)
public class JpaUserRepository implements IUserRepository {
    public JpaUserRepository(JpaUserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void addUser(User user) {
        userDao.save(UserEntity.from(user));
    }

    @Override
    public boolean hasUser(String username) {
        return userDao.existsByUsername(username);
    }

    @Override
    public User getUser(String username) {
        assert hasUser(username) : "The user '%s' does not exist.".formatted(username);

        return userDao.findByUsername(username).toUser();
    }

    private final JpaUserDao userDao;
}
