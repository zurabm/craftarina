package com.craftarina.webstore.infra.jpa.entities;

import com.craftarina.webstore.core.products.Product;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = ProductEntity.TABLE)
public class ProductEntity {
    public static final String TABLE = "products";

    @Id
    private String name;
    private double price;
    private String description;
    private int stock;

    /**
     * Creates a {@link ProductEntity} to be stored in a repository from the specified {@link Product}.
     */
    public static ProductEntity from(Product product) {
        ProductEntity entity = new ProductEntity();
        entity.name = product.name();
        entity.price = product.price();
        entity.description = product.description();
        entity.stock = product.stock();

        return entity;
    }

    /**
     * Converts this {@link ProductEntity} to a {@link Product}.
     */
    public Product toProduct() {
        return new Product(getName(), getPrice(), getDescription(), getStock());
    }

    @Override
    public String toString() {
        return "ProductEntity{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", stock=" + stock +
                '}';
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public int getStock() {
        return stock;
    }
}
