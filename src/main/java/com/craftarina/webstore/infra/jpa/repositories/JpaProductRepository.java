package com.craftarina.webstore.infra.jpa.repositories;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.repositories.IProductRepository;
import com.craftarina.webstore.infra.jpa.daos.JpaProductDao;
import com.craftarina.webstore.infra.jpa.daos.JpaProductImageDao;
import com.craftarina.webstore.infra.jpa.entities.ProductEntity;
import com.craftarina.webstore.infra.jpa.entities.ProductImageEntity;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.nio.ByteBuffer;

import static com.craftarina.webstore.infra.spring.configurations.Profiles.PERSISTENCE_PROFILE;


@Repository
@Profile(PERSISTENCE_PROFILE)
public class JpaProductRepository implements IProductRepository {
    public JpaProductRepository(JpaProductDao productDao, JpaProductImageDao productImageDao) {
        this.productDao = productDao;
        this.productImageDao = productImageDao;
    }

    @Override
    public Iterable<Product> getProducts() {
        return productDao.findAll().stream().map(ProductEntity::toProduct).toList();
    }

    @Override
    public void addProduct(Product product) {
        assert !productDao.existsByName(product.name()) : "A product with the name '%s' already exists".formatted(product.name());

        productDao.save(ProductEntity.from(product));
    }

    @Override
    public void addProductImage(String productName, ByteBuffer productImage) {
        assert productDao.existsByName(productName) : "A product with the name '%s' does not exist.".formatted(productName);
        assert !productImageDao.existsByProductName(productName) : "The product '%s' already has an image.".formatted(productName);

        productImageDao.save(ProductImageEntity.from(productName, productImage));
    }

    @Override
    public boolean hasProductImage(String productName) {
        assert productDao.existsByName(productName) : "A product with the name '%s' does not exist.".formatted(productName);

        return productImageDao.existsByProductName(productName);
    }

    @Override
    public ByteBuffer getProductImage(String productName) {
        assert productDao.existsByName(productName) : "A product with the name '%s' does not exist.".formatted(productName);
        assert productImageDao.existsByProductName(productName) : "The product '%s' does not have an image.".formatted(productName);

        return productImageDao.findByProductName(productName).getProductImage();
    }

    private final JpaProductDao productDao;
    private final JpaProductImageDao productImageDao;
}
