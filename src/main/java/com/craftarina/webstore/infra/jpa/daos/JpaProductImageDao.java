package com.craftarina.webstore.infra.jpa.daos;

import com.craftarina.webstore.infra.jpa.entities.ProductEntity;
import com.craftarina.webstore.infra.jpa.entities.ProductImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaProductImageDao extends JpaRepository<ProductImageEntity, ProductEntity> {
    /**
     * Returns true if a product image with the specified name exists.
     */
    boolean existsByProductName(String productName);

    /**
     * Returns a {@link ProductImageEntity} corresponding to the specified product.
     */
    ProductImageEntity findByProductName(String productName);
}
