package com.craftarina.webstore.infra.jpa.entities;

import javax.persistence.*;
import java.nio.ByteBuffer;

@Entity
@Table(name = ProductImageEntity.TABLE)
@SuppressWarnings("unused")
public class ProductImageEntity {
    public static final String TABLE = "product_images";

    @Id
    private String productName;

    @Lob
    private byte[] productImage;

    @OneToOne
    @PrimaryKeyJoinColumn
    private ProductEntity product;

    /**
     * Creates a {@link ProductImageEntity} from the specified product name and image.
     */
    public static ProductImageEntity from(String productName, ByteBuffer productImage) {
        ProductImageEntity entity = new ProductImageEntity();
        entity.productName = productName;
        entity.productImage = productImage.array();

        return entity;
    }

    public ByteBuffer getProductImage() {
        return ByteBuffer.wrap(productImage);
    }
}
