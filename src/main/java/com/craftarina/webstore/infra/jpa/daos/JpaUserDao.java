package com.craftarina.webstore.infra.jpa.daos;

import com.craftarina.webstore.infra.jpa.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaUserDao extends JpaRepository<UserEntity, String> {
    /**
     * Returns true if a user with the specified username exists.
     */
    boolean existsByUsername(String username);

    /**
     * Returns a {@link UserEntity} with the specified username.
     */
    UserEntity findByUsername(String username);
}
