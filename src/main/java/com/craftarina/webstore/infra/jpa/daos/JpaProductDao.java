package com.craftarina.webstore.infra.jpa.daos;

import com.craftarina.webstore.infra.jpa.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaProductDao extends JpaRepository<ProductEntity, String> {
    /**
     * Returns true if a product with the specified name exists.
     */
    boolean existsByName(String name);
}
