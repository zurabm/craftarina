package com.craftarina.webstore.infra.jpa.entities;

import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.core.users.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = UserEntity.TABLE)
public class UserEntity {
    public static final String TABLE = "users";

    @Id
    private String username;
    private String password;
    private String role;

    /**
     * Creates a {@link UserEntity} to be stored in a repository from the specified {@link User}.
     */
    public static UserEntity from(User user) {
        UserEntity entity = new UserEntity();
        entity.username = user.username();
        entity.password = user.password();
        entity.role = user.role().getRoleName();

        return entity;
    }

    /**
     * Converts this {@link UserEntity} to a {@link User}.
     */
    public User toUser() {
        return new User(username, password, Role.fromName(role));
    }

    @Override
    public String toString() {
        return "UserEntity{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                '}';
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }
}
