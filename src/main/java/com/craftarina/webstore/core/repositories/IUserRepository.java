package com.craftarina.webstore.core.repositories;

import com.craftarina.webstore.core.users.User;

public interface IUserRepository {
    /**
     * Add the specified user to this user repository.
     */
    void addUser(User user);

    /**
     * Returns true if a user with the specified username exists.
     */
    boolean hasUser(String username);

    /**
     * Returns the details of the user with the specified username.
     */
    User getUser(String username);
}
