package com.craftarina.webstore.core.repositories;

import com.craftarina.webstore.core.carts.Cart;

public interface ICartRepository {
    /**
     * Stores a cart for the specified user.
     */
    void addCart(String username, Cart cart);

    /**
     * Returns true if the specified user has an active shopping cart.
     */
    boolean hasCart(String username);

    /**
     * Returns the shopping cart for the specified user.
     */
    Cart getCart(String username);

    /**
     * Updates the cart belonging to the specified user.
     */
    void updateCart(String username, Cart cart);

    /**
     * Removes the cart belonging to the specified user.
     */
    void removeCart(String username);
}
