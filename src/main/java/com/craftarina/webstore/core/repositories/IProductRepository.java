package com.craftarina.webstore.core.repositories;

import com.craftarina.webstore.core.products.Product;

import java.nio.ByteBuffer;

public interface IProductRepository {
    /**
     * Returns an {@link Iterable} over all the products in this repository.
     */
    Iterable<Product> getProducts();

    /**
     * Adds the specified product to this product repository.
     */
    void addProduct(Product product);

    /**
     * Adds an image of the specified product.
     */
    void addProductImage(String productName, ByteBuffer productImage);

    /**
     * Returns true if an image for the specified product exists.
     */
    boolean hasProductImage(String productName);

    /**
     * Returns the image corresponding to the specified product as a {@link ByteBuffer}.
     */
    ByteBuffer getProductImage(String productName);
}
