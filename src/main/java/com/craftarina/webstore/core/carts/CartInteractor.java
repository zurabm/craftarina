package com.craftarina.webstore.core.carts;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.repositories.ICartRepository;

import java.util.Collection;
import java.util.List;

public class CartInteractor {
    public CartInteractor(ICartRepository cartRepository) {
        this.cartRepository = cartRepository;
    }

    /**
     * Creates an empty shopping cart for the specified user.
     */
    public void addCart(String username) {
        cartRepository.addCart(username, new Cart());
    }

    /**
     * Returns the active shopping cart for the specified user.
     *
     * @throws NoActiveCartException if the specified user does not have an active cart.
     */
    public Cart getActiveCart(String username) {
        if (!cartRepository.hasCart(username)) {
            throw new NoActiveCartException("The user '%s' does not have an active shopping cart.".formatted(username));
        }

        return cartRepository.getCart(username);
    }

    /**
     * Adds the specified products to the active cart of the specified user.
     */
    public void addProducts(String username, Collection<Product> products) {
        cartRepository.updateCart(username, cartRepository.getCart(username).addProducts(products));
    }

    /**
     * Removes all the specified products from the given user's cart.
     */
    public void removeProducts(String username, List<Product> products) {
        cartRepository.updateCart(username, cartRepository.getCart(username).removeProducts(products));
    }

    /**
     * Removes the currently active cart for the specified user.
     */
    public void removeCart(String username) {
        cartRepository.removeCart(username);
    }

    private final ICartRepository cartRepository;
}
