package com.craftarina.webstore.core.carts;

public class NoActiveCartException extends RuntimeException {
    public NoActiveCartException(String message) {
        super(message);
    }
}
