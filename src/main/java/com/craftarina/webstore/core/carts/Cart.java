package com.craftarina.webstore.core.carts;

import com.craftarina.webstore.core.products.Product;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public record Cart(ImmutableList<Product> products) {
    /**
     * Creates an empty shopping cart.
     */
    public Cart() {
        this(ImmutableList.of());
    }

    /**
     * Creates a new shopping cart with the specified products added.
     */
    public Cart addProducts(Collection<Product> targetProducts) {
        List<Product> updatedProducts = new ArrayList<>(products());
        updatedProducts.addAll(targetProducts);

        return new Cart(ImmutableList.copyOf(updatedProducts));
    }

    /**
     * Creates a new shopping cart with the specified products removed.
     */
    public Cart removeProducts(List<Product> targetProducts) {
        List<Product> updatedProducts = new ArrayList<>(products());
        updatedProducts.removeAll(targetProducts);

        return new Cart(ImmutableList.copyOf(updatedProducts));
    }
}
