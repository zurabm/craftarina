package com.craftarina.webstore.core.users;

import java.util.Arrays;

public enum Role {
    USER("User"),
    ADMIN("Admin");

    /**
     * Returns the {@link Role} enum corresponding to the specified name.
     */
    public static Role fromName(String roleName) {
        return Arrays.stream(Role.values())
                .filter(role -> role.getRoleName().equals(roleName))
                .findFirst().orElseThrow();
    }

    /**
     * Returns the name of this role as a {@link String}.
     */
    public String getRoleName() {
        return roleName;
    }

    Role(String roleName) {
        this.roleName = roleName;
    }

    private final String roleName;
}
