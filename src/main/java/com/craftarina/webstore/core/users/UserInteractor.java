package com.craftarina.webstore.core.users;

import com.craftarina.webstore.core.repositories.IUserRepository;

public class UserInteractor {
    public UserInteractor(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * Creates a user with the specified details.
     *
     * @throws InvalidUsernameException if the user with the specified username already exists.
     */
    public void addUser(User user) {
        if (userRepository.hasUser(user.username())) {
            throw new InvalidUsernameException("The user '%s' already exists.".formatted(user.username()));
        }

        userRepository.addUser(user);
    }

    /**
     * Retrieves the user with the specified username.
     *
     * @throws InvalidUsernameException if the user with the specified username does not exist.
     */
    public User getUser(String username) {
        if (!userRepository.hasUser(username)) {
            throw new InvalidUsernameException("The user '%s' does not exist.".formatted(username));
        }

        return userRepository.getUser(username);
    }

    private final IUserRepository userRepository;
}
