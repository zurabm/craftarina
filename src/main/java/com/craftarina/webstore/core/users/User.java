package com.craftarina.webstore.core.users;

public record User(String username, String password, Role role) {
    /**
     * Creates a {@link User} with the default role ("User").
     */
    public User(String username, String password) {
        this(username, password, Role.USER);
    }
}
