package com.craftarina.webstore.core.cookies;

import java.net.HttpCookie;
import java.util.Collection;
import java.util.stream.Collectors;

public class Cookies {
    /**
     * Parses cookies from the specified strings and combines them into a single string. The result
     * can be directly set as the value for the "Cookies" header.
     *
     * @param cookieStrings An array of cookie strings. Each string must contain exactly 1 cookie.
     */
    public static String toCookieHeader(Collection<String> cookieStrings) {
        return cookieStrings.stream()
                .map(HttpCookie::parse)
                .map(httpCookie -> httpCookie.get(0).toString())
                .collect(Collectors.joining("; "));
    }
}
