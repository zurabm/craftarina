package com.craftarina.webstore.core.services;

import com.craftarina.webstore.core.products.MissingImageException;
import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.products.ProductInteractor;
import com.craftarina.webstore.core.users.InvalidUsernameException;
import com.craftarina.webstore.core.users.User;
import com.craftarina.webstore.core.users.UserInteractor;

import java.nio.ByteBuffer;

public class CraftarinaService {
    public CraftarinaService(UserInteractor userInteractor, ProductInteractor productInteractor) {
        this.userInteractor = userInteractor;
        this.productInteractor = productInteractor;
    }

    /**
     * Creates a user with the specified details.
     *
     * @throws InvalidUsernameException if the user with the specified username already exists.
     */
    public void addUser(User user) {
        userInteractor.addUser(user);
    }

    /**
     * Retrieves the user with the specified username.
     *
     * @throws InvalidUsernameException if the user with the specified username does not exist.
     */
    public User getUser(String username) {
        return userInteractor.getUser(username);
    }

    /**
     * Returns all products from the store's catalog.
     */
    public Iterable<Product> getProducts() {
        return productInteractor.getProducts();
    }

    /**
     * Adds the specified product to the store's catalog.
     */
    public void addProduct(Product product) {
        productInteractor.addProduct(product);
    }

    /**
     * Adds the given image for the specified product.
     */
    public void addProductImage(String productName, ByteBuffer productImage) {
        productInteractor.addProductImage(productName, productImage);
    }

    /**
     * Returns the image corresponding to the specified product.
     *
     * @throws MissingImageException if the specified product does not have an image.
     */
    public ByteBuffer getProductImage(String productName) {
        return productInteractor.getProductImage(productName);
    }

    private final UserInteractor userInteractor;
    private final ProductInteractor productInteractor;
}
