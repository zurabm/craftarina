package com.craftarina.webstore.core.products;

public record Product(String name, double price, String description, int stock) {
}
