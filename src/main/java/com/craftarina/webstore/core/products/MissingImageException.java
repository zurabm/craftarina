package com.craftarina.webstore.core.products;

public class MissingImageException extends RuntimeException {
    public MissingImageException(String message) {
        super(message);
    }
}
