package com.craftarina.webstore.core.products;

import com.craftarina.webstore.core.repositories.IProductRepository;

import java.nio.ByteBuffer;

public class ProductInteractor {
    public ProductInteractor(IProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Adds the specified product to the store's catalog.
     */
    public void addProduct(Product product) {
        productRepository.addProduct(product);
    }

    /**
     * Returns all products from the store's catalog.
     */
    public Iterable<Product> getProducts() {
        return productRepository.getProducts();
    }

    /**
     * Adds the given image for the specified product.
     */
    public void addProductImage(String productName, ByteBuffer productImage) {
        productRepository.addProductImage(productName, productImage);
    }

    /**
     * Returns the image corresponding to the specified product.
     *
     * @throws MissingImageException if the specified product does not have an image.
     */
    public ByteBuffer getProductImage(String productName) {
        if (!productRepository.hasProductImage(productName)) {
            throw new MissingImageException("An image for the product '%s' does not exist.".formatted(productName));
        }

        return productRepository.getProductImage(productName);
    }

    private final IProductRepository productRepository;
}
