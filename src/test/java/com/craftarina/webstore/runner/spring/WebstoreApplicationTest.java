package com.craftarina.webstore.runner.spring;

import org.junit.jupiter.api.Test;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;

import javax.sql.DataSource;

import static com.craftarina.webstore.infra.spring.configurations.Profiles.PERSISTENCE_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.Profiles.SECURITY_PROFILE;

@SpringBootTest
@ActiveProfiles({SECURITY_PROFILE, PERSISTENCE_PROFILE})
class WebstoreApplicationTest {
    @Test
    void contextLoads() {
    }

    @Primary
    @TestConfiguration
    static class Configuration {
        static final String DB_DRIVER = "org.h2.Driver";
        static final String DB_USER = "";
        static final String DB_PASSWORD = "";
        static final String DB_URL = "jdbc:h2:mem:test";

        @Bean
        public DataSource getDataSource() {
            return DataSourceBuilder.create()
                    .driverClassName(DB_DRIVER)
                    .username(DB_USER)
                    .password(DB_PASSWORD)
                    .url(DB_URL)
                    .build();
        }
    }
}
