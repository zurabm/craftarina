package com.craftarina.webstore;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.core.users.User;

import static com.craftarina.webstore.Randoms.*;

public class Models {
    /**
     * Creates a test user with some random field values and the specified credentials.
     */
    public static User testUser(String username, String password) {
        return testUser(username, password, Role.USER);
    }

    /**
     * Creates a test user with some random field values, the specified credentials and role.
     */
    public static User testUser(String username, String password, Role role) {
        return new User(username, password, role);
    }

    /**
     * Creates a test product with some random field values and the specified name.
     */
    public static Product testProduct(String name) {
        return new Product(name, randomFloat(), randomString(), randomInt());
    }
}
