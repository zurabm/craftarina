package com.craftarina.webstore;

import com.craftarina.webstore.core.users.Role;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

public class Randoms {
    private static final Logger LOGGER = LoggerFactory.getLogger(Randoms.class);

    public static final Random random = new Random();
    public static final float DEFAULT_MAX__FLOAT_VALUE = Float.MAX_VALUE;
    public static final int DEFAULT_RANDOM_STRING_LENGTH = 20;

    public static final String[] SAMPLE_IMAGES = {
            "src/test/resources/images/toucan.jpeg",
            "src/test/resources/images/alexander-hamilton.jpg"
    };

    /**
     * Generates a random integer.
     */
    public static int randomInt() {
        return random.nextInt();
    }

    /**
     * Generates a random positive floating point number.
     */
    public static float randomFloat() {
        return random.nextFloat() * DEFAULT_MAX__FLOAT_VALUE;
    }

    /**
     * Generates a random {@link String} of a fixed length.
     */
    public static String randomString() {
        return RandomStringUtils.random(DEFAULT_RANDOM_STRING_LENGTH, true, true);
    }

    /**
     * Generates a random {@link Role}.
     */
    public static Role randomRole() {
        return Role.values()[random.nextInt(Role.values().length)];
    }

    /**
     * Generates a random Base64 encoded image, as a {@link ByteBuffer}.
     */
    public static ByteBuffer randomImage() {
        String imagePath = SAMPLE_IMAGES[random.nextInt(SAMPLE_IMAGES.length)];

        try {
            return ByteBuffer.wrap(FileUtils.readFileToByteArray(new File(imagePath)));
        } catch (IOException e) {
            LOGGER.error("Couldn't retrieve test image at '{}'.", imagePath, e);
            throw new RuntimeException("A test image is missing.");
        }
    }
}
