package com.craftarina.webstore.infra.jpa.repositories;

import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.core.users.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import static com.craftarina.webstore.Models.testUser;
import static com.craftarina.webstore.Randoms.randomRole;
import static com.craftarina.webstore.Randoms.randomString;
import static com.craftarina.webstore.infra.spring.configurations.Profiles.PERSISTENCE_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
@EntityScan(basePackages = {"com.craftarina.webstore.infra.jpa.entities"})
@EnableJpaRepositories(basePackages = {"com.craftarina.webstore.infra.jpa.daos"})
@ContextConfiguration(classes = JpaUserRepository.class)
@ActiveProfiles(PERSISTENCE_PROFILE)
class JpaUserRepositoryTest {
    @Autowired
    private JpaUserRepository userRepository;

    @Test
    void testShouldAddUserToRepository() {
        User user = testUser("User 1", "Some password", Role.USER);
        userRepository.addUser(user);

        assertThat(userRepository.hasUser("User 1")).isTrue();
        assertThat(userRepository.getUser("User 1")).isEqualTo(user);
    }

    @Test
    void testShouldAddMultipleUsersToRepository() {
        User user1 = testUser(randomString(), randomString(), randomRole());
        User user2 = testUser(randomString(), randomString(), randomRole());
        User user3 = testUser(randomString(), randomString(), randomRole());

        userRepository.addUser(user1);
        userRepository.addUser(user2);
        userRepository.addUser(user3);

        assertThat(userRepository.hasUser(user1.username())).isTrue();
        assertThat(userRepository.hasUser(user2.username())).isTrue();
        assertThat(userRepository.hasUser(user3.username())).isTrue();

        assertThat(userRepository.getUser(user3.username())).isEqualTo(user3);
        assertThat(userRepository.getUser(user2.username())).isEqualTo(user2);
        assertThat(userRepository.getUser(user1.username())).isEqualTo(user1);
    }
}
