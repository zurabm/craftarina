package com.craftarina.webstore.infra.jpa.repositories;

import com.craftarina.webstore.core.products.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.nio.ByteBuffer;

import static com.craftarina.webstore.Models.testProduct;
import static com.craftarina.webstore.Randoms.randomImage;
import static com.craftarina.webstore.infra.spring.configurations.Profiles.PERSISTENCE_PROFILE;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@EntityScan(basePackages = {"com.craftarina.webstore.infra.jpa.entities"})
@EnableJpaRepositories(basePackages = {"com.craftarina.webstore.infra.jpa.daos"})
@ContextConfiguration(classes = JpaProductRepository.class)
@ActiveProfiles(PERSISTENCE_PROFILE)
class JpaProductRepositoryTest {
    @Autowired
    private JpaProductRepository productRepository;

    @Test
    void testShouldCreateEmptyRepository() {
        assertThat(productRepository.getProducts()).isEmpty();
    }

    @Test
    void testShouldAddProductToRepository() {
        Product product = testProduct("Some Product");
        productRepository.addProduct(product);

        assertThat(productRepository.getProducts()).containsExactly(product);
    }

    @Test
    void testShouldAddMultipleProductsToRepository() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);

        assertThat(productRepository.getProducts()).containsExactly(product1, product2, product3);
    }

    @Test
    void testShouldAddProductImageToRepository() {
        Product product = testProduct("Product");
        ByteBuffer productImage = randomImage();

        productRepository.addProduct(product);
        productRepository.addProductImage(product.name(), productImage);

        assertThat(productRepository.hasProductImage(product.name())).isTrue();
        assertThat(productRepository.getProductImage(product.name())).isEqualTo(productImage);
    }

    @Test
    void testShouldAddMultipleProductImagesToRepository() {
        Product product1 = testProduct("Test Product 1");
        Product product2 = testProduct("Test Product 2");
        Product product3 = testProduct("Test Product 3");
        ByteBuffer productImage1 = randomImage();
        ByteBuffer productImage2 = randomImage();
        ByteBuffer productImage3 = randomImage();

        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);
        productRepository.addProductImage(product1.name(), productImage1);
        productRepository.addProductImage(product2.name(), productImage2);
        productRepository.addProductImage(product3.name(), productImage3);

        assertThat(productRepository.hasProductImage(product1.name())).isTrue();
        assertThat(productRepository.hasProductImage(product2.name())).isTrue();
        assertThat(productRepository.hasProductImage(product3.name())).isTrue();
        assertThat(productRepository.getProductImage(product1.name())).isEqualTo(productImage1);
        assertThat(productRepository.getProductImage(product2.name())).isEqualTo(productImage2);
        assertThat(productRepository.getProductImage(product3.name())).isEqualTo(productImage3);
    }
}
