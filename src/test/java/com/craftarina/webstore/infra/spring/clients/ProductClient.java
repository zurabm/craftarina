package com.craftarina.webstore.infra.spring.clients;

import com.craftarina.webstore.infra.spring.controllers.products.AddProductImageRequest;
import com.craftarina.webstore.infra.spring.controllers.products.AddProductRequest;
import com.craftarina.webstore.infra.spring.controllers.products.ProductModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.ByteBuffer;

import static com.craftarina.webstore.infra.spring.controllers.products.ProductController.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;

public record ProductClient(MockMvc mvc) {
    /**
     * Submits a GET request to fetch all products.
     */
    public MvcResult fetchProducts() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get(FETCH_PRODUCTS_URL)).andReturn();
    }

    /**
     * Submits a POST request to create a product.
     */
    public MvcResult addProduct(ProductModel product) throws Exception {
        return addProduct(product, true);
    }

    /**
     * Submits a POST request to create a product. An invalid CSRF token can optionally be used.
     */
    public MvcResult addProduct(ProductModel product, boolean validCsrf) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post(ADD_PRODUCT_URL)
                .with(validCsrf ? csrf() : csrf().useInvalidToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new AddProductRequest(product)))
        ).andReturn();
    }

    /**
     * Submits a GET request to fetch the image for the specified product.
     */
    public MvcResult fetchProductImage(String productName) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get(FETCH_PRODUCT_IMAGE_URL, productName)).andReturn();
    }

    /**
     * Submits a POST request to upload an image for the specified product.
     */
    public MvcResult addProductImage(String productName, ByteBuffer productImage) throws Exception {
        return addProductImage(productName, productImage, true);
    }

    /**
     * Submits a POST request to upload an image for the specified product. An invalid CSRF token
     * can optionally be used.
     */
    public MvcResult addProductImage(String productName, ByteBuffer productImage, boolean validCsrf) throws Exception {
        return mvc.perform(
                MockMvcRequestBuilders.post(ADD_PRODUCT_IMAGE_URL, productName)
                        .with(validCsrf ? csrf() : csrf().useInvalidToken())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(
                                new AddProductImageRequest(productImage.array())
                        ))
        ).andReturn();
    }

    public static ProductClient from(MockMvc mvc) {
        return new ProductClient(mvc);
    }
}
