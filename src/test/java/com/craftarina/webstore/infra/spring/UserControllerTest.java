package com.craftarina.webstore.infra.spring;

import com.craftarina.webstore.infra.repositories.InMemoryUserRepository;
import com.craftarina.webstore.infra.spring.clients.UserClient;
import com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration;
import com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration;
import com.craftarina.webstore.infra.spring.configurations.WebstoreConfiguration;
import com.craftarina.webstore.infra.spring.controllers.users.UserController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration.IN_MEMORY_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration.NO_SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.expectStatus;
import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest
@ContextConfiguration(classes = {
        UserController.class,
        InMemoryConfiguration.class,
        WebstoreConfiguration.class,
        NoSecurityConfiguration.class,
})
@ActiveProfiles({NO_SECURITY_PROFILE, IN_MEMORY_PROFILE})
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private UserClient userClient;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private InMemoryUserRepository userRepository;

    @BeforeEach
    void setUp() {
        userClient = UserClient.from(mockMvc);
        userRepository.clear();
    }

    @Test
    void testShouldRegisterUser() throws Exception {
        MvcResult result = userClient.addUser("User", "Password");

        assertThat(userRepository.hasUser("User")).isTrue();
        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldRegisterMultipleUsers() throws Exception {
        MvcResult result_1 = userClient.addUser("User 1", "Password 1");
        MvcResult result_2 = userClient.addUser("User 2", "Password 2");
        MvcResult result_3 = userClient.addUser("User 3", "Password 1");

        assertThat(userRepository.hasUser("User 1")).isTrue();
        assertThat(userRepository.hasUser("User 2")).isTrue();
        assertThat(userRepository.hasUser("User 3")).isTrue();
        expectStatus(result_1.getResponse(), HttpStatus.CREATED);
        expectStatus(result_2.getResponse(), HttpStatus.CREATED);
        expectStatus(result_3.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldStoreEncodedUserPassword() throws Exception {
        userClient.addUser("Test", "pass");

        assertThat(encoder.matches("pass", userRepository.getUser("Test").password())).isTrue();
    }
}
