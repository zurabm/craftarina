package com.craftarina.webstore.infra.spring.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ResponseUtils {
    /**
     * Parses a java object of the specified type from the JSON content of the {@link MvcResult}.
     */
    public static <T> T parseResponse(MvcResult result, Class<T> responseType)
            throws UnsupportedEncodingException, JsonProcessingException {

        return new ObjectMapper().readValue(result.getResponse().getContentAsString(), responseType);
    }

    /**
     * Verifies that the {@link HttpServletResponse} has the specified status code.
     */
    public static void expectStatus(HttpServletResponse response, HttpStatus expectedStatus) {
        assertThat(response.getStatus()).isEqualTo(expectedStatus.value());
    }

    /**
     * Verifies that the {@link ResponseEntity} has the specified status code.
     */
    public static void expectStatus(ResponseEntity<?> response, HttpStatus expectedStatus) {
        assertThat(response.getStatusCode()).isEqualTo(expectedStatus);
    }

    /**
     * Verifies that the {@link HttpClientErrorException} has the specified status code.
     */
    public static void expectStatus(HttpClientErrorException exception, HttpStatus expectedStatus) {
        assertThat(exception.getStatusCode()).isEqualTo(expectedStatus);
    }
}
