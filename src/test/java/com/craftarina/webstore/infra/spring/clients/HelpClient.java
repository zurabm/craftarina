package com.craftarina.webstore.infra.spring.clients;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.craftarina.webstore.infra.spring.controllers.help.HelpController.FETCH_HELP_URL;


public record HelpClient(MockMvc mvc) {
    /**
     * Submits a GET request to fetch a help message.
     */
    public MvcResult fetchHelp() throws Exception {
        return mvc.perform(MockMvcRequestBuilders.get(FETCH_HELP_URL)).andReturn();
    }

    public static HelpClient from(MockMvc mvc) {
        return new HelpClient(mvc);
    }
}
