package com.craftarina.webstore.infra.spring;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.infra.repositories.InMemoryProductRepository;
import com.craftarina.webstore.infra.spring.clients.ProductClient;
import com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration;
import com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration;
import com.craftarina.webstore.infra.spring.configurations.WebstoreConfiguration;
import com.craftarina.webstore.infra.spring.controllers.products.FetchProductImageResponse;
import com.craftarina.webstore.infra.spring.controllers.products.FetchProductsResponse;
import com.craftarina.webstore.infra.spring.controllers.products.ProductController;
import com.craftarina.webstore.infra.spring.controllers.products.ProductModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.nio.ByteBuffer;

import static com.craftarina.webstore.Models.testProduct;
import static com.craftarina.webstore.Randoms.randomImage;
import static com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration.IN_MEMORY_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration.NO_SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.expectStatus;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.parseResponse;
import static org.assertj.core.api.Assertions.assertThat;

@WebMvcTest
@ContextConfiguration(classes = {
        ProductController.class,
        InMemoryConfiguration.class,
        WebstoreConfiguration.class,
        NoSecurityConfiguration.class,
})
@ActiveProfiles({NO_SECURITY_PROFILE, IN_MEMORY_PROFILE})
public class ProductControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private ProductClient productClient;

    @Autowired
    private InMemoryProductRepository productRepository;

    @BeforeEach
    void setUp() {
        productClient = ProductClient.from(mockMvc);
        productRepository.clearProducts();
    }

    @Test
    void testShouldReturnEmptyProductCatalog() throws Exception {
        MvcResult result = productClient.fetchProducts();

        assertThat(parseResponse(result, FetchProductsResponse.class).getProducts()).isEmpty();
        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldReturnProductsCatalog() throws Exception {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);
        MvcResult result = productClient.fetchProducts();

        assertThat(parseResponse(result, FetchProductsResponse.class).getProducts())
                .containsExactly(
                        ProductModel.from(product1),
                        ProductModel.from(product2),
                        ProductModel.from(product3)
                );
        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldAddOneProductToCatalog() throws Exception {
        Product product = testProduct("Test Product");

        MvcResult result = productClient.addProduct(ProductModel.from(product));

        assertThat(productRepository.getProducts()).containsExactly(product);
        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldAddProductsToCatalog() throws Exception {
        Product product1 = testProduct("Test Product 1");
        Product product2 = testProduct("Test Product 2");

        MvcResult result_1 = productClient.addProduct(ProductModel.from(product1));
        MvcResult result_2 = productClient.addProduct(ProductModel.from(product2));

        assertThat(productRepository.getProducts()).containsExactly(product1, product2);

        expectStatus(result_1.getResponse(), HttpStatus.CREATED);
        expectStatus(result_2.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldNotFetchMissingProductImage() throws Exception {
        Product product = testProduct("Product");
        productClient.addProduct(ProductModel.from(product));

        MvcResult result = productClient.fetchProductImage(product.name());

        expectStatus(result.getResponse(), HttpStatus.NOT_FOUND);
    }

    @Test
    void testShouldFetchProductImage() throws Exception {
        Product product = testProduct("Product");
        ByteBuffer productImage = randomImage();

        productRepository.addProduct(product);
        productRepository.addProductImage(product.name(), productImage);

        MvcResult result = productClient.fetchProductImage(product.name());
        ByteBuffer fetchedImage = ByteBuffer.wrap(parseResponse(result, FetchProductImageResponse.class).getImageBytes());

        assertThat(fetchedImage).isEqualTo(productImage);
        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldAddProductImage() throws Exception {
        Product product = testProduct("Product");
        ByteBuffer productImage = randomImage();
        productClient.addProduct(ProductModel.from(product));

        MvcResult result = productClient.addProductImage(product.name(), productImage);

        assertThat(productRepository.getProductImage(product.name())).isEqualTo(productImage);
        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldAddMultipleProductImages() throws Exception {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");
        ByteBuffer productImage1 = randomImage();
        ByteBuffer productImage2 = randomImage();
        ByteBuffer productImage3 = randomImage();

        productClient.addProduct(ProductModel.from(product1));
        productClient.addProduct(ProductModel.from(product2));
        productClient.addProduct(ProductModel.from(product3));

        MvcResult result1 = productClient.addProductImage(product1.name(), productImage1);
        MvcResult result2 = productClient.addProductImage(product2.name(), productImage2);
        MvcResult result3 = productClient.addProductImage(product3.name(), productImage3);

        assertThat(productRepository.getProductImage(product1.name())).isEqualTo(productImage1);
        assertThat(productRepository.getProductImage(product2.name())).isEqualTo(productImage2);
        assertThat(productRepository.getProductImage(product3.name())).isEqualTo(productImage3);

        expectStatus(result1.getResponse(), HttpStatus.CREATED);
        expectStatus(result2.getResponse(), HttpStatus.CREATED);
        expectStatus(result3.getResponse(), HttpStatus.CREATED);
    }
}

