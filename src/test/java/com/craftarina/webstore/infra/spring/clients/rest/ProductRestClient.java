package com.craftarina.webstore.infra.spring.clients.rest;

import com.craftarina.webstore.infra.spring.controllers.products.AddProductRequest;
import com.craftarina.webstore.infra.spring.controllers.products.ProductModel;
import org.springframework.http.ResponseEntity;

import static com.craftarina.webstore.infra.spring.controllers.products.ProductController.ADD_PRODUCT_URL;

public record ProductRestClient(RestClient client) {
    /**
     * Submits a POST request to create a product.
     */
    public ResponseEntity<Void> addProduct(ProductModel product) {
        return client().post(ADD_PRODUCT_URL, new AddProductRequest(product), Void.class);
    }

    public static ProductRestClient from(RestClient restClient) {
        return new ProductRestClient(restClient);
    }
}
