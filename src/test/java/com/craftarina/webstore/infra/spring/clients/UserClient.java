package com.craftarina.webstore.infra.spring.clients;

import com.craftarina.webstore.infra.spring.controllers.users.AddUserRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.craftarina.webstore.infra.spring.controllers.users.UserController.ADD_USER_URL;

public record UserClient(MockMvc mvc) {
    /**
     * Submits a POST request to create a user.
     */
    public MvcResult addUser(String username, String password) throws Exception {
        return mvc.perform(MockMvcRequestBuilders.post(ADD_USER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(new AddUserRequest(username, password)))
        ).andReturn();
    }

    public static UserClient from(MockMvc mvc) {
        return new UserClient(mvc);
    }
}
