package com.craftarina.webstore.infra.spring;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.core.users.User;
import com.craftarina.webstore.infra.repositories.InMemoryUserRepository;
import com.craftarina.webstore.infra.spring.clients.rest.ProductRestClient;
import com.craftarina.webstore.infra.spring.clients.rest.RestClient;
import com.craftarina.webstore.infra.spring.clients.rest.SessionRestClient;
import com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration;
import com.craftarina.webstore.infra.spring.controllers.products.ProductModel;
import com.craftarina.webstore.runner.spring.WebstoreApplication;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;

import javax.annotation.PostConstruct;

import static com.craftarina.webstore.Models.testProduct;
import static com.craftarina.webstore.Randoms.randomString;
import static com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration.IN_MEMORY_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.Profiles.SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.expectStatus;
import static org.junit.jupiter.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT, classes = {
        WebstoreApplication.class,
        InMemoryConfiguration.class,
        AuthenticationTest.Configuration.class
})
@ActiveProfiles({SECURITY_PROFILE, IN_MEMORY_PROFILE})
public class AuthenticationTest {
    @Value("${local.server.port}")
    private int port;

    private SessionRestClient sessionClient;
    private ProductRestClient productClient;

    @BeforeEach
    void setUp() {
        RestClient restClient = RestClient.forPort(port);
        sessionClient = SessionRestClient.from(restClient);
        productClient = ProductRestClient.from(restClient);
    }

    @Test
    void testShouldAuthenticateExistingUserWithCorrectCredentials() {
        ResponseEntity<Void> response = sessionClient.fetchSession("User", "User password");

        expectStatus(response, HttpStatus.OK);
    }

    @Test()
    void testShouldNotAuthenticateExistingUserWithIncorrectCredentials() {
        try {
            sessionClient.fetchSession("User", "Wrong User password");
            fail("a client error exception should have been thrown.");
        } catch (HttpClientErrorException exception) {
            expectStatus(exception, HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void testShouldNotAuthenticateNonExistentUser() {
        try {
            sessionClient.fetchSession("James Bond", "It's a secret");
            fail("a client error exception should have been thrown.");
        } catch (HttpClientErrorException exception) {
            expectStatus(exception, HttpStatus.UNAUTHORIZED);
        }
    }

    @Test
    void testShouldAllowAccessToAuthorisedResourceAfterAuthentication() {
        sessionClient.fetchSession("Admin", "Admin password");

        Product product = testProduct(randomString());
        ResponseEntity<Void> response = productClient.addProduct(ProductModel.from(product));

        expectStatus(response, HttpStatus.CREATED);
    }

    @Test
    void testShouldAllowMultipleAccessesToAuthorisedResourceAfterAuthentication() {
        sessionClient.fetchSession("Admin", "Admin password");

        Product product1 = testProduct("Test Product 1");
        Product product2 = testProduct("Test Product 2");
        Product product3 = testProduct("Test Product 3");

        ResponseEntity<Void> response1 = productClient.addProduct(ProductModel.from(product1));
        ResponseEntity<Void> response2 = productClient.addProduct(ProductModel.from(product2));
        ResponseEntity<Void> response3 = productClient.addProduct(ProductModel.from(product3));

        expectStatus(response1, HttpStatus.CREATED);
        expectStatus(response2, HttpStatus.CREATED);
        expectStatus(response3, HttpStatus.CREATED);
    }

    @EnableWebSecurity
    static class Configuration extends WebSecurityConfigurerAdapter {

        public Configuration(InMemoryUserRepository userRepository, PasswordEncoder encoder) {
            this.userRepository = userRepository;
            this.encoder = encoder;
        }

        @PostConstruct
        public void setUpUserRepository() {
            userRepository.addUser(new User("User", encoder.encode("User password"), Role.USER));
            userRepository.addUser(new User("Admin", encoder.encode("Admin password"), Role.ADMIN));
        }

        private final InMemoryUserRepository userRepository;
        private final PasswordEncoder encoder;
    }
}
