package com.craftarina.webstore.infra.spring;

import com.craftarina.webstore.infra.spring.clients.HelpClient;
import com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration;
import com.craftarina.webstore.infra.spring.controllers.help.FetchHelpResponse;
import com.craftarina.webstore.infra.spring.controllers.help.HelpController;
import com.craftarina.webstore.infra.spring.utils.ResponseUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration.IN_MEMORY_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.NoSecurityConfiguration.NO_SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.expectStatus;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@WebMvcTest
@ContextConfiguration(classes = {HelpController.class, NoSecurityConfiguration.class})
@ActiveProfiles({NO_SECURITY_PROFILE, IN_MEMORY_PROFILE})
class HelpControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private HelpClient client;

    @BeforeEach
    void setUp() {
        client = HelpClient.from(mockMvc);
    }

    @Test
    void testShouldReturnHelpMessage() throws Exception {
        MvcResult result = client.fetchHelp();

        String helpMessage = ResponseUtils.parseResponse(result, FetchHelpResponse.class).getHelpMessage();

        assertThat(helpMessage).isNotNull().isNotBlank();
        expectStatus(result.getResponse(), HttpStatus.OK);
    }
}
