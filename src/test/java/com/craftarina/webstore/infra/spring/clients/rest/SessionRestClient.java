package com.craftarina.webstore.infra.spring.clients.rest;

import com.craftarina.webstore.infra.spring.controllers.sessions.FetchSessionRequest;
import org.springframework.http.ResponseEntity;

import static com.craftarina.webstore.infra.spring.controllers.sessions.SessionController.FETCH_SESSION_URL;

public record SessionRestClient(RestClient client) {
    /**
     * Submits a POST request to authenticate a user and create a session.
     */
    public ResponseEntity<Void> fetchSession(String username, String password) {
        return client().post(FETCH_SESSION_URL, new FetchSessionRequest(username, password), Void.class);
    }

    public static SessionRestClient from(RestClient restClient) {
        return new SessionRestClient(restClient);
    }
}
