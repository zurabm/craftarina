package com.craftarina.webstore.infra.spring.clients.rest;

import com.craftarina.webstore.core.cookies.Cookies;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

public class RestClient {
    public static final String COOKIE_HEADER = "Cookie";
    public static final String SET_COOKIE_HEADER = "set-cookie";

    /**
     * Submits a POST request to the specified URL.
     */
    public <ResponseT, RequestT> ResponseEntity<ResponseT> post(
            String url, RequestT requestBody, Class<ResponseT> responseClass
    ) {
        ResponseEntity<ResponseT> response = new RestTemplate().postForEntity(
                "http://localhost:{port}" + url,
                new HttpEntity<>(requestBody, withCookieHeader(new HttpHeaders())),
                responseClass,
                port
        );

        storeCookies(response);

        return response;
    }

    /**
     * Creates a generic REST client that will submit requests to specified port of localhost.
     */
    public static RestClient forPort(int port) {
        return new RestClient(port);
    }

    private RestClient(int port) {
        this.port = port;
    }

    /**
     * Stores cookies from the specified {@link ResponseEntity}.
     */
    private void storeCookies(ResponseEntity<?> response) {
        List<String> cookieStrings = response.getHeaders().get(SET_COOKIE_HEADER);

        if (cookieStrings != null) {
            cookieHeader = Cookies.toCookieHeader(cookieStrings);
        }
    }

    /**
     * Adds the most recently stored cookies (if any) to the specified {@link HttpHeaders}.
     */
    private HttpHeaders withCookieHeader(HttpHeaders headers) {
        if (cookieHeader != null) {
            headers.add(COOKIE_HEADER, cookieHeader);
        }
        return headers;
    }

    private String cookieHeader;
    private final int port;
}
