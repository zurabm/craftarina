package com.craftarina.webstore.infra.spring.configurations;

import com.craftarina.webstore.infra.repositories.InMemoryProductRepository;
import com.craftarina.webstore.infra.repositories.InMemoryUserRepository;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@TestConfiguration
@Profile(InMemoryConfiguration.IN_MEMORY_PROFILE)
public class InMemoryConfiguration {
    public static final String IN_MEMORY_PROFILE = "in-memory";

    @Bean
    public InMemoryUserRepository getUserRepository() {
        return new InMemoryUserRepository();
    }

    @Bean
    public InMemoryProductRepository getProductRepository() {
        return new InMemoryProductRepository();
    }
}
