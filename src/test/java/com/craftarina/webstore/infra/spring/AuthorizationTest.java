package com.craftarina.webstore.infra.spring;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.core.users.Role;
import com.craftarina.webstore.core.users.User;
import com.craftarina.webstore.infra.repositories.InMemoryProductRepository;
import com.craftarina.webstore.infra.repositories.InMemoryUserRepository;
import com.craftarina.webstore.infra.spring.clients.HelpClient;
import com.craftarina.webstore.infra.spring.clients.ProductClient;
import com.craftarina.webstore.infra.spring.clients.UserClient;
import com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration;
import com.craftarina.webstore.infra.spring.configurations.SecurityConfiguration;
import com.craftarina.webstore.infra.spring.configurations.WebstoreConfiguration;
import com.craftarina.webstore.infra.spring.controllers.help.HelpController;
import com.craftarina.webstore.infra.spring.controllers.products.ProductController;
import com.craftarina.webstore.infra.spring.controllers.products.ProductModel;
import com.craftarina.webstore.infra.spring.controllers.users.UserController;
import com.craftarina.webstore.infra.spring.security.WebstoreUserDetailsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.annotation.PostConstruct;

import static com.craftarina.webstore.Models.testProduct;
import static com.craftarina.webstore.Randoms.randomImage;
import static com.craftarina.webstore.Randoms.randomString;
import static com.craftarina.webstore.infra.spring.configurations.InMemoryConfiguration.IN_MEMORY_PROFILE;
import static com.craftarina.webstore.infra.spring.configurations.Profiles.SECURITY_PROFILE;
import static com.craftarina.webstore.infra.spring.utils.ResponseUtils.expectStatus;

@WebMvcTest
@ContextConfiguration(classes = {
        SecurityConfiguration.class,
        WebstoreUserDetailsService.class,
        AuthorizationTest.Configuration.class,
        HelpController.class,
        UserController.class,
        ProductController.class,
        InMemoryConfiguration.class,
        WebstoreConfiguration.class,
})
@ActiveProfiles({SECURITY_PROFILE, IN_MEMORY_PROFILE})
class AuthorizationTest {
    @Autowired
    private MockMvc mockMvc;
    private HelpClient helpClient;
    private UserClient userClient;
    private ProductClient productClient;

    @Autowired
    private InMemoryProductRepository productRepository;

    @BeforeEach
    void setUp() {
        helpClient = HelpClient.from(mockMvc);
        userClient = UserClient.from(mockMvc);
        productClient = ProductClient.from(mockMvc);

        productRepository.clearProducts();
    }

    @Test
    void testShouldGetHelpMessageWithoutAuthentication() throws Exception {
        MvcResult result = helpClient.fetchHelp();

        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldRegisterUserWithoutAuthentication() throws Exception {
        MvcResult result = userClient.addUser(randomString(), randomString());

        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    void testShouldGetProductCatalogWithoutAuthentication() throws Exception {
        MvcResult result = productClient.fetchProducts();

        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldGetProductImageWithoutAuthentication() throws Exception {
        productRepository.addProduct(testProduct("Product"));
        productRepository.addProductImage("Product", randomImage());

        MvcResult result = productClient.fetchProductImage("Product");

        expectStatus(result.getResponse(), HttpStatus.OK);
    }

    @Test
    void testShouldNotAddProductToCatalogWithoutAuthentication() throws Exception {
        Product product = testProduct(randomString());
        MvcResult result = productClient.addProduct(ProductModel.from(product));

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @Test
    void testShouldNotAddProductImageWithoutAuthentication() throws Exception {
        productRepository.addProduct(testProduct("Product"));

        MvcResult result = productClient.addProductImage("Product", randomImage());

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("Joe")
    void testShouldNotAddProductToCatalogWithoutAdminAuthority() throws Exception {
        Product product = testProduct(randomString());
        MvcResult result = productClient.addProduct(ProductModel.from(product));

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("Joe")
    void testShouldNotAddProductImageWithoutAdminAuthority() throws Exception {
        productRepository.addProduct(testProduct("Product"));

        MvcResult result = productClient.addProductImage("Product", randomImage());

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("Admin")
    void testShouldAddProductToCatalogWithAdminAuthority() throws Exception {
        Product product = testProduct(randomString());
        MvcResult result = productClient.addProduct(ProductModel.from(product));

        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    @WithUserDetails("Admin")
    void testShouldNotAddProductToCatalogWithInvalidCsrf() throws Exception {
        Product product = testProduct(randomString());
        MvcResult result = productClient.addProduct(ProductModel.from(product), false);

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @Test
    @WithUserDetails("Admin")
    void testShouldAddProductImageWithAdminAuthority() throws Exception {
        productRepository.addProduct(testProduct("Product"));

        MvcResult result = productClient.addProductImage("Product", randomImage());

        expectStatus(result.getResponse(), HttpStatus.CREATED);
    }

    @Test
    @WithUserDetails("Admin")
    void testShouldNotAddProductImageWithInvalidCsrf() throws Exception {
        productRepository.addProduct(testProduct("Product"));

        MvcResult result = productClient.addProductImage("Product", randomImage(), false);

        expectStatus(result.getResponse(), HttpStatus.FORBIDDEN);
    }

    @EnableWebSecurity
    static class Configuration extends WebSecurityConfigurerAdapter {

        public Configuration(InMemoryUserRepository userRepository) {
            this.userRepository = userRepository;
        }

        @PostConstruct
        public void setUpUserRepository() {
            userRepository.addUser(new User("Joe", randomString(), Role.USER));
            userRepository.addUser(new User("Admin", randomString(), Role.ADMIN));
        }

        private final InMemoryUserRepository userRepository;
    }
}
