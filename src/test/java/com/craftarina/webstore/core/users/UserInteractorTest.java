package com.craftarina.webstore.core.users;

import com.craftarina.webstore.infra.repositories.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.craftarina.webstore.Models.testUser;
import static com.craftarina.webstore.Randoms.randomString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class UserInteractorTest {
    private UserInteractor userInteractor;

    @BeforeEach
    void setUp() {
        userInteractor = new UserInteractor(new InMemoryUserRepository());
    }

    @Test
    void testShouldRegisterUser() {
        userInteractor.addUser(new User(randomString(), randomString()));
    }

    @Test
    void testShouldRetrieveRegisteredUser() {
        User user = testUser(randomString(), randomString());
        userInteractor.addUser(user);

        assertThat(userInteractor.getUser(user.username())).isEqualTo(user);
    }

    @Test
    void testShouldRetrieveMultipleRegisteredUsers() {
        User user1 = testUser("user 1", randomString());
        User user2 = testUser("user 2", randomString());
        User user3 = testUser("user 3", randomString());

        userInteractor.addUser(user1);
        userInteractor.addUser(user2);
        userInteractor.addUser(user3);

        assertThat(userInteractor.getUser("user 3")).isEqualTo(user3);
        assertThat(userInteractor.getUser("user 2")).isEqualTo(user2);
        assertThat(userInteractor.getUser("user 1")).isEqualTo(user1);
    }

    @Test
    void testShouldNotRetrieveNonExistentUser() {
        userInteractor.addUser(testUser("user 1", randomString()));
        userInteractor.addUser(testUser("user 2", randomString()));

        assertThatExceptionOfType(InvalidUsernameException.class)
                .isThrownBy(() -> userInteractor.getUser("Random User"));
    }

    @Test
    void testShouldNotCreateUsersWithDuplicateUsernames() {
        userInteractor.addUser(testUser("user", randomString()));

        assertThatExceptionOfType(InvalidUsernameException.class)
                .isThrownBy(() -> userInteractor.addUser(testUser("user", randomString())));
    }
}
