package com.craftarina.webstore.core.cookies;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CookiesTest {

    @Test
    void testShouldStripCookieFlagsFromSingleCookie() {
        assertThat(
                Cookies.toCookieHeader(List.of("JSESSIONID=Value; Path=/some-path; HttpOnly"))
        ).isEqualTo("JSESSIONID=Value");
    }

    @Test
    void testShouldStripCookieFlagsFromMultipleCookies() {
        assertThat(Cookies.toCookieHeader(List.of(
                "JSESSIONID=Value; Path=/some-path; HttpOnly",
                "X-XSRF-TOKEN=Token; Path=/some-other-path; HttpOnly"
        ))).isEqualTo("JSESSIONID=Value; X-XSRF-TOKEN=Token");
    }
}
