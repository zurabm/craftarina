package com.craftarina.webstore.core.products;

import com.craftarina.webstore.infra.repositories.InMemoryProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.ByteBuffer;

import static com.craftarina.webstore.Models.testProduct;
import static com.craftarina.webstore.Randoms.randomImage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;


class ProductInteractorTest {
    private InMemoryProductRepository productRepository;
    private ProductInteractor productInteractor;

    @BeforeEach
    void setUp() {
        productRepository = new InMemoryProductRepository();
        productInteractor = new ProductInteractor(productRepository);
    }

    @Test
    void testShouldReturnProductCatalog() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        productRepository.addProduct(product1);
        productRepository.addProduct(product2);
        productRepository.addProduct(product3);

        assertThat(productInteractor.getProducts()).containsExactly(product1, product2, product3);
    }

    @Test
    void testShouldAddProductsToCatalog() {
        Product product1 = testProduct("Product A");
        Product product2 = testProduct("Product B");
        Product product3 = testProduct("Product C");

        productInteractor.addProduct(product1);
        productInteractor.addProduct(product2);
        productInteractor.addProduct(product3);

        assertThat(productRepository.getProducts()).containsExactly(product1, product2, product3);
    }

    @Test
    void testShouldNotRetrieveMissingImage() {
        Product product = testProduct("Product A");
        productInteractor.addProduct(product);

        assertThatExceptionOfType(MissingImageException.class)
                .isThrownBy(() -> productInteractor.getProductImage(product.name()));
    }

    @Test
    void testShouldAddImageForProduct() {
        Product product = testProduct("Product");
        ByteBuffer productImage = randomImage();

        productInteractor.addProduct(product);
        productInteractor.addProductImage(product.name(), productImage);

        assertThat(productInteractor.getProductImage(product.name())).isEqualTo(productImage);
    }
}
