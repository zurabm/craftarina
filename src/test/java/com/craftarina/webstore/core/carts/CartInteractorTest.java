package com.craftarina.webstore.core.carts;

import com.craftarina.webstore.core.products.Product;
import com.craftarina.webstore.infra.repositories.InMemoryCartRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.craftarina.webstore.Models.testProduct;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class CartInteractorTest {
    private InMemoryCartRepository cartRepository;
    private CartInteractor cartInteractor;

    @BeforeEach
    void setUp() {
        cartRepository = new InMemoryCartRepository();
        cartInteractor = new CartInteractor(cartRepository);
    }

    @Test
    void testShouldCreateEmptyCart() {
        cartInteractor.addCart("User");

        assertThat(cartInteractor.getActiveCart("User").products()).isEmpty();
    }

    @Test
    void testShouldCreateEmptyCartForMultipleUsers() {
        cartInteractor.addCart("User 1");
        cartInteractor.addCart("User 2");
        cartInteractor.addCart("User 3");

        assertThat(cartInteractor.getActiveCart("User 1").products()).isEmpty();
        assertThat(cartInteractor.getActiveCart("User 2").products()).isEmpty();
        assertThat(cartInteractor.getActiveCart("User 3").products()).isEmpty();
    }

    @Test
    void testShouldNotFindCartForUsersWithoutCarts() {
        assertThatExceptionOfType(NoActiveCartException.class)
                .isThrownBy(() -> cartInteractor.getActiveCart("User"));
    }

    @Test
    void testShouldAddProductsToCart() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User");
        cartInteractor.addProducts("User", List.of(product1, product2, product3));

        assertThat(cartInteractor.getActiveCart("User").products())
                .containsExactlyInAnyOrder(product1, product2, product3);
    }

    @Test
    void testShouldAddProductsToCartForMultipleUsers() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User 1");
        cartInteractor.addCart("User 2");

        cartInteractor.addProducts("User 1", List.of(product2, product3));
        cartInteractor.addProducts("User 2", List.of(product1, product2));

        assertThat(cartInteractor.getActiveCart("User 1").products())
                .containsExactlyInAnyOrder(product2, product3);

        assertThat(cartInteractor.getActiveCart("User 2").products())
                .containsExactlyInAnyOrder(product1, product2);
    }

    @Test
    void testShouldRemoveProductsFromCart() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User");
        cartInteractor.addProducts("User", List.of(product1, product2, product3));
        cartInteractor.removeProducts("User", List.of(product1));

        assertThat(cartInteractor.getActiveCart("User").products())
                .containsExactlyInAnyOrder(product2, product3);
    }

    @Test
    void testShouldRemoveProductsFromCartForMultipleUsers() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User 1");
        cartInteractor.addCart("User 2");

        cartInteractor.addProducts("User 1", List.of(product1, product2, product3));
        cartInteractor.addProducts("User 2", List.of(product1, product2, product3));

        cartInteractor.removeProducts("User 1", List.of(product1, product2));
        cartInteractor.removeProducts("User 2", List.of(product3, product2));

        assertThat(cartInteractor.getActiveCart("User 1").products()).containsExactlyInAnyOrder(product3);
        assertThat(cartInteractor.getActiveCart("User 2").products()).containsExactlyInAnyOrder(product1);
    }

    @Test
    void testShouldRemoveCart() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User");
        cartInteractor.addProducts("User", List.of(product1, product2, product3));
        cartInteractor.removeCart("User");

        assertThatExceptionOfType(NoActiveCartException.class)
                .isThrownBy(() -> cartInteractor.getActiveCart("User"));
    }

    @Test
    void testShouldStoreCartsPersistently() {
        Product product1 = testProduct("Product 1");
        Product product2 = testProduct("Product 2");
        Product product3 = testProduct("Product 3");

        cartInteractor.addCart("User");
        cartInteractor.addProducts("User", List.of(product1, product2, product3));

        assertThat(new CartInteractor(cartRepository).getActiveCart("User").products())
                .containsExactlyInAnyOrder(product1, product2, product3);
    }
}
