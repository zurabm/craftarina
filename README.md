# Craftarina Web Store

## Description

This is a small web store for selling hand-knitted goods.

## User Stories

> As a *user*, I would like to be able to browse available products, so that I can see what's available.

> As a *user*, I would like to be able to add products to a cart, so that I can purchase them later.

> As a *user*, I would like to be able to view my shopping cart, so that I can keep track of what I am purchasing.

> As a *user*, I would like to be able to pay by cash or by credit card, so that I can choose whichever is convenient.

> As a *user*, I would like to be able to mark products as favorites, so that I can browse them later.

> As a *user*, I would like to be able to message support from the store's web page.

> As an *administrator*, I would like to be able to add products to the store's catalog, so users can purchase them.

> As an *administrator*, I would like to be able to view the store's statistics.

## Technical Details

* Anyone visiting the website can browse the store's catalog.
* Users wishing to add items to their cart will need to register.
* Users should be able to view their shopping cart at any time without having to leave the page.
* Users should be able to specify and modify the quantity of each product.
* Users should be able to remove products from their cart.
* Users should be able to see the price of each product, as well as the total cost for their cart.
* The availability of chosen products should be verified before checkout.
* Users should be able to view a list of their favorite products.
* Users should be able to remove products from their favorites list.
* Users should be able to message the store support through a tab available on every page.
* Products in the store's catalog should contain the following:
    * An image of the product.
    * The name of the product.
    * The product tags.
    * The price.
    * The description.
    * The number of this product left in stock.
* The administrator should be able to edit or remove existing products.
* The administrator should be able to view:
    * A list of available products.
    * A list of sold-out products.
    * The number of a specific product left in stock.
    * A list of store transactions.
    * The count of total items sold.
    * The total revenue.

### User Credentials

The user credentials consist of:

* The username.
* The password.

## Web Store API

> POST: /users

* Registers a new user.
* Should contain user credentials in the request body.
* Required Authorization: None.

> POST: /sessions

* Authenticates an existing user.
* Should contain user credentials in the request body.
* Returns the authentication result.
* Required Authorization: None.

> GET: /products

* Returns all products available in the store's catalog.
* Required Authorization: None.

> GET: /products/images/{product_name}

* Returns the image corresponding to the specified product.
* Required Authorization: None.

## Web Store Admin API

> POST: /products

* Adds a new product to the store's catalog.
* Contains product information in request body (without the image).
* Required Authorization: administrator.

> POST: /products/images/{product_name}

* Adds an image for an existing product in the store's catalog.
* Contains the image as a Base64 encoded string in the request body.
* Required Authorization: administrator.
